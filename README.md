# Welcome! #

Welcome to your first Android Wear Watch Face! This repository holds an incremental walk-through of how to get started creating your first watch face for Android Wear 2.0. See the presentation below for more information.

### Geek Girl Tech Con 2017 - [Link to Presentation](https://docs.google.com/presentation/d/1U9IZQGMixwo6gdYt-vBcfI_LCmTHnw9NHi5YjVz5e_Q/edit?usp=sharing) ###

### Android Wear Documentation ###

* Start here (tutorials) - [Creating Watch Faces](https://developer.android.com/training/wearables/watch-faces/index.html)
* Watch face style guide - [Style guide](https://developer.android.com/design/wear/watchfaces.html)
* Dealing with round vs square and chin - [Addressing Common Issues](https://developer.android.com/training/wearables/watch-faces/issues.html)
* Canvas methods - [Canvas](https://developer.android.com/reference/android/graphics/Canvas.html)

### Kotlin Resources ###
* Google's resource page on Kotlin - [Start here](https://developer.android.com/kotlin/resources.html)
* Main Kotlin features - [Kotlin idioms](https://kotlinlang.org/docs/reference/idioms.html)
* Intro to Kotlin at Google I/O - [Video](https://www.youtube.com/watch?v=X1RVYt2QKQE)
* Android Development with Kotlin - [Video](https://www.youtube.com/watch?v=A2LukgT2mKc)

### Other Resources ###

* Pairing emulators - [StackOverflow](http://stackoverflow.com/questions/25205888/pairing-android-and-wear-emulators)
* What's new in Android Wear 2.0 - [Video](https://www.youtube.com/watch?v=-F-llkD6cQY)
* LoopJ - [Main page](http://loopj.com/android-async-http/) - [Example setup](https://guides.codepath.com/android/Using-Android-Async-Http-Client)
* Picasso - [Main page](http://square.github.io/picasso/)
* Join the Android Wear Developers group - [Google+](https://plus.google.com/communities/113381227473021565406)

### Sparkistic Watch Faces ###

* [Just A Minute](https://play.google.com/store/apps/details?id=com.sparkistic.justaminute&utm_source=bitbucket&utm_medium=social&utm_campaign=socalcodecamp)
* [Just A Minute - Pride Edition](https://play.google.com/store/apps/details?id=com.sparkistic.justaminutepride&utm_source=bitbucket&utm_medium=social&utm_campaign=socalcodecamp)
* [PhotoWear](https://play.google.com/store/apps/details?id=com.sparkistic.photowear&utm_source=bitbucket&utm_medium=social&utm_campaign=socalcodecamp)