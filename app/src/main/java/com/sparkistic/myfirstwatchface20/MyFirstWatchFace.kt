/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sparkistic.myfirstwatchface20

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.graphics.Palette
import android.support.wearable.watchface.CanvasWatchFaceService
import android.support.wearable.watchface.WatchFaceService
import android.support.wearable.watchface.WatchFaceStyle
import android.view.SurfaceHolder
import android.view.WindowInsets
import android.widget.Toast
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.RequestParams
import com.loopj.android.http.TextHttpResponseHandler
import com.squareup.picasso.Picasso
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.TimeUnit











/**
 * Analog watch face with a ticking second hand. In ambient mode, the second hand isn't
 * shown. On devices with low-bit ambient mode, the hands are drawn without anti-aliasing in ambient
 * mode. The watch face is drawn with less contrast in mute mode.
 */
class MyFirstWatchFace : CanvasWatchFaceService() {

    private val HOUR_STROKE_WIDTH = 5f
    private val MINUTE_STROKE_WIDTH = 3f
    private val SECOND_TICK_STROKE_WIDTH = 2f
    private val CENTER_GAP_AND_CIRCLE_RADIUS = 4f
    private val SHADOW_RADIUS = 6

    override fun onCreateEngine(): Engine {
        return Engine()
    }

    private class EngineHandler(reference: MyFirstWatchFace.Engine) : Handler() {
        private val mWeakReference: WeakReference<MyFirstWatchFace.Engine> = WeakReference(reference)

        override fun handleMessage(msg: Message) {
            val engine = mWeakReference.get()
            if (engine != null) {
                when (msg.what) {
                    MSG_UPDATE_TIME -> engine.handleUpdateTimeMessage()
                }
            }
        }
    }

    inner class Engine : CanvasWatchFaceService.Engine() {
        private val mPeekCardBounds = Rect()
        /* Handler to update the time once a second in interactive mode. */
        private val mUpdateTimeHandler = EngineHandler(this)
        private var mCalendar: Calendar? = null
        private val mTimeZoneReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                mCalendar!!.timeZone = TimeZone.getDefault()
                invalidate()
            }
        }
        val dogMessageReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                dogName = DOGS.valueOf(intent.getStringExtra("dog"))
                when (dogName) {
                    DOGS.ERNIE -> mBackgroundBitmap = BitmapFactory.decodeResource(resources, R.drawable.bg_ernie)
                    DOGS.LUCY -> mBackgroundBitmap = BitmapFactory.decodeResource(resources, R.drawable.bg_lucy)
                }
                rescaleImage()
                recolorWatchFaceHands()
                invalidate()
            }
        }
        private var dogName: DOGS = DOGS.ERNIE
        private var mRegisteredTimeZoneReceiver = false
        private var mMuteMode: Boolean = false
        private var mCenterX: Float = 0.toFloat()
        private var mCenterY: Float = 0.toFloat()
        private var mSecondHandLength: Float = 0.toFloat()
        private var sMinuteHandLength: Float = 0.toFloat()
        private var sHourHandLength: Float = 0.toFloat()
        /* Colors for all hands (hour, minute, seconds, ticks) based on photo loaded. */
        private var mWatchHandColor: Int = 0
        private var mWatchHandHighlightColor: Int = 0
        private var mWatchHandShadowColor: Int = 0
        private var mHourPaint: Paint? = null
        private var mMinutePaint: Paint? = null
        private var mSecondPaint: Paint? = null
        private var mTickAndCirclePaint: Paint? = null
        private var mBackgroundPaint: Paint? = null
        private var mBackgroundBitmap: Bitmap? = null
        private var mGrayBackgroundBitmap: Bitmap? = null
        private var mAmbient: Boolean = false
        private var mLowBitAmbient: Boolean = false
        private var mBurnInProtection: Boolean = false
        private var mWidth: Int = 0
        private var mHeight: Int = 0
        private var mTouchRadius: Float = 0f
        private var mTouchX: Float = 0f
        private var mTouchY: Float = 0f
        private var mHandPaint: Paint = Paint()
        private var mChinSize: Int = 0

        override fun onCreate(holder: SurfaceHolder?) {
            super.onCreate(holder)

            setWatchFaceStyle(WatchFaceStyle.Builder(this@MyFirstWatchFace)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .setAcceptsTapEvents(true)
                    .build())

            mBackgroundPaint = Paint()
            mBackgroundPaint!!.color = Color.BLACK
            mBackgroundBitmap = BitmapFactory.decodeResource(resources, R.drawable.bg_ernie)

            /* Set defaults for colors */
            mWatchHandColor = Color.WHITE
            mWatchHandHighlightColor = Color.RED
            mWatchHandShadowColor = Color.BLACK

            mHourPaint = Paint()
            mHourPaint!!.color = mWatchHandColor
            mHourPaint!!.strokeWidth = HOUR_STROKE_WIDTH
            mHourPaint!!.isAntiAlias = true
            mHourPaint!!.strokeCap = Paint.Cap.ROUND
            mHourPaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)

            mMinutePaint = Paint()
            mMinutePaint!!.color = mWatchHandColor
            mMinutePaint!!.strokeWidth = MINUTE_STROKE_WIDTH
            mMinutePaint!!.isAntiAlias = true
            mMinutePaint!!.strokeCap = Paint.Cap.ROUND
            mMinutePaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)

            mSecondPaint = Paint()
            mSecondPaint!!.color = mWatchHandHighlightColor
            mSecondPaint!!.strokeWidth = SECOND_TICK_STROKE_WIDTH
            mSecondPaint!!.isAntiAlias = true
            mSecondPaint!!.strokeCap = Paint.Cap.ROUND
            mSecondPaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)

            mTickAndCirclePaint = Paint()
            mTickAndCirclePaint!!.color = mWatchHandColor
            mTickAndCirclePaint!!.strokeWidth = SECOND_TICK_STROKE_WIDTH
            mTickAndCirclePaint!!.isAntiAlias = true
            mTickAndCirclePaint!!.style = Paint.Style.STROKE
            mTickAndCirclePaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)

            /* Extract colors from background image to improve watchface style. */
            recolorWatchFaceHands()

            mCalendar = Calendar.getInstance()

            LocalBroadcastManager.getInstance(this@MyFirstWatchFace).registerReceiver(dogMessageReceiver,
                    IntentFilter(WatchFaceWearableConfigActivity.DOG_CHANGE_EVENT))

            mHandPaint.style = Paint.Style.STROKE
            mHandPaint.color = Color.WHITE
            mHandPaint.strokeWidth = resources.getDimension(R.dimen.analog_hand_stroke)
            mHandPaint.isAntiAlias = true
            mHandPaint.strokeCap = Paint.Cap.ROUND
        }

        private fun recolorWatchFaceHands() {
            Palette.from(mBackgroundBitmap!!).generate { palette ->
                if (palette != null) {
                    mWatchHandHighlightColor = palette.getVibrantColor(Color.RED)
                    mWatchHandColor = palette.getLightVibrantColor(Color.WHITE)
                    mWatchHandShadowColor = palette.getDarkMutedColor(Color.BLACK)
                    mHandPaint.color = palette.getLightMutedColor(Color.WHITE)
                    updateWatchHandStyle()
                }
            }
        }

        override fun onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            LocalBroadcastManager.getInstance(this@MyFirstWatchFace).unregisterReceiver(dogMessageReceiver);
            super.onDestroy()
        }

        override fun onPropertiesChanged(properties: Bundle?) {
            super.onPropertiesChanged(properties)
            mLowBitAmbient = properties!!.getBoolean(WatchFaceService.PROPERTY_LOW_BIT_AMBIENT, false)
            mBurnInProtection = properties.getBoolean(WatchFaceService.PROPERTY_BURN_IN_PROTECTION, false)
        }

        override fun onTimeTick() {
            super.onTimeTick()
            invalidate()
        }

        override fun onApplyWindowInsets(insets: WindowInsets) {
            super.onApplyWindowInsets(insets)
            mChinSize = insets.systemWindowInsetBottom
        }

        override fun onAmbientModeChanged(inAmbientMode: Boolean) {
            super.onAmbientModeChanged(inAmbientMode)
            mAmbient = inAmbientMode

            updateWatchHandStyle()

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer()
        }

        private fun updateWatchHandStyle() {
            if (mAmbient) {
                mHourPaint!!.color = Color.WHITE
                mMinutePaint!!.color = Color.WHITE
                mSecondPaint!!.color = Color.WHITE
                mTickAndCirclePaint!!.color = Color.WHITE

                mHourPaint!!.isAntiAlias = false
                mMinutePaint!!.isAntiAlias = false
                mSecondPaint!!.isAntiAlias = false
                mTickAndCirclePaint!!.isAntiAlias = false

                mHourPaint!!.clearShadowLayer()
                mMinutePaint!!.clearShadowLayer()
                mSecondPaint!!.clearShadowLayer()
                mTickAndCirclePaint!!.clearShadowLayer()

            } else {
                mHourPaint!!.color = mWatchHandColor
                mMinutePaint!!.color = mWatchHandColor
                mSecondPaint!!.color = mWatchHandHighlightColor
                mTickAndCirclePaint!!.color = mWatchHandColor

                mHourPaint!!.isAntiAlias = true
                mMinutePaint!!.isAntiAlias = true
                mSecondPaint!!.isAntiAlias = true
                mTickAndCirclePaint!!.isAntiAlias = true

                mHourPaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)
                mMinutePaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)
                mSecondPaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)
                mTickAndCirclePaint!!.setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, mWatchHandShadowColor)
            }
        }

        override fun onInterruptionFilterChanged(interruptionFilter: Int) {
            super.onInterruptionFilterChanged(interruptionFilter)
            val inMuteMode = interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE

            /* Dim display in mute mode. */
            if (mMuteMode != inMuteMode) {
                mMuteMode = inMuteMode
                mHourPaint!!.alpha = if (inMuteMode) 100 else 255
                mMinutePaint!!.alpha = if (inMuteMode) 100 else 255
                mSecondPaint!!.alpha = if (inMuteMode) 80 else 255
                invalidate()
            }
        }


        override fun onSurfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            super.onSurfaceChanged(holder, format, width, height)

            /*
             * Find the coordinates of the center point on the screen, and ignore the window
             * insets, so that, on round watches with a "chin", the watch face is centered on the
             * entire screen, not just the usable portion.
             */
            mCenterX = width / 2f
            mCenterY = height / 2f
            mWidth = width
            mHeight = height
            /*
             * Calculate lengths of different hands based on watch screen size.
             */
            mSecondHandLength = (mCenterX * 0.875).toFloat()
            sMinuteHandLength = (mCenterX * 0.75).toFloat()
            sHourHandLength = (mCenterX * 0.5).toFloat()


            /* Scale loaded background image (more efficient) if surface dimensions change. */
            rescaleImage()

            /*
             * Create a gray version of the image only if it will look nice on the device in
             * ambient mode. That means we don't want devices that support burn-in
             * protection (slight movements in pixels, not great for images going all the way to
             * edges) and low ambient mode (degrades image quality).
             *
             * Also, if your watch face will know about all images ahead of time (users aren't
             * selecting their own photos for the watch face), it will be more
             * efficient to create a black/white version (png, etc.) and load that when you need it.
             */
            if (!mBurnInProtection && !mLowBitAmbient) {
                initGrayBackgroundBitmap()
            }
        }

        private fun rescaleImage() {
            val w = mBackgroundBitmap!!.width
            val h = mBackgroundBitmap!!.height
            val scale: Float
            if (w >= h) {
                scale = mHeight.toFloat() / h.toFloat()
            } else {
                scale = mWidth.toFloat() / w.toFloat()
            }

            mBackgroundBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap,
                    (w * scale).toInt(),
                    (h * scale).toInt(), true)
        }

        private fun initGrayBackgroundBitmap() {
            mGrayBackgroundBitmap = Bitmap.createBitmap(
                    mBackgroundBitmap!!.width,
                    mBackgroundBitmap!!.height,
                    Bitmap.Config.ARGB_8888)
            val canvas = Canvas(mGrayBackgroundBitmap!!)
            val grayPaint = Paint()
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            val filter = ColorMatrixColorFilter(colorMatrix)
            grayPaint.colorFilter = filter
            canvas.drawBitmap(mBackgroundBitmap!!, 0f, 0f, grayPaint)
        }

        /* Determine if touch is inside circle */
        fun isTouchInsideCircle(x: Int, y: Int): Boolean {
            val distance = Math.sqrt(((x - mTouchX) * (x - mTouchX) + (y - mTouchY) * (y - mTouchY)).toDouble()).toFloat()
            return distance < mTouchRadius
        }

        private val target = object : com.squareup.picasso.Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                mBackgroundBitmap = bitmap
                rescaleImage()
                recolorWatchFaceHands()
                invalidate()
            }

            override fun onBitmapFailed(errorDrawable: Drawable) {

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable) {

            }
        }
        /**
         * Captures tap event (and tap type). The [WatchFaceService.TAP_TYPE_TAP] case can be
         * used for implementing specific logic to handle the gesture.
         */
        override fun onTapCommand(tapType: Int, x: Int, y: Int, eventTime: Long) {
            when (tapType) {
                WatchFaceService.TAP_TYPE_TOUCH -> {
                }
                WatchFaceService.TAP_TYPE_TOUCH_CANCEL -> {
                }
                WatchFaceService.TAP_TYPE_TAP ->
                    // The user has completed the tap gesture.
                    if (isTouchInsideCircle(x, y)) {
                        Toast.makeText(applicationContext, R.string.loading_message, Toast.LENGTH_SHORT).show()
                        fetchImageOfTheDay()
                    }
            }// The user has started touching the screen.
            // The user has started a different gesture or otherwise cancelled the tap.
            invalidate()
        }

        private fun fetchImageOfTheDay() {
            val client = AsyncHttpClient()
            val params = RequestParams()
            params.put("api_key", "NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo")
            client.get("https://api.nasa.gov/planetary/apod", params, object : TextHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseString: String?) {
                    val jObject = JSONObject(responseString)
                    val url = jObject.getString("url")
                    Picasso.with(applicationContext)
                            .load(url)
                            .placeholder(R.drawable.bg_lucy)
                            .error(R.drawable.bg_lucy)
                            .into(target)
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                    Toast.makeText(applicationContext, R.string.error_message, Toast.LENGTH_SHORT).show()
                }

            })
        }

        override fun onDraw(canvas: Canvas?, bounds: Rect?) {
            val now = System.currentTimeMillis()
            mCalendar!!.timeInMillis = now

            if (mAmbient && (mLowBitAmbient || mBurnInProtection)) {
                canvas!!.drawColor(Color.BLACK)
            } else if (mAmbient) {
                canvas!!.drawBitmap(mGrayBackgroundBitmap!!, 0f, 0f, mBackgroundPaint)
            } else {
                canvas!!.drawBitmap(mBackgroundBitmap!!, 0f, 0f, mBackgroundPaint)
            }

            /* Draw touch circle to change dog background */
            mTouchRadius = bounds!!.width() * 0.1f
            mTouchX = mCenterX
            mTouchY = bounds.height() - mTouchRadius * 2f
            canvas.drawCircle(mTouchX, mTouchY, mTouchRadius, mHandPaint)

            /*
             * Draw ticks. Usually you will want to bake this directly into the photo, but in
             * cases where you want to allow users to select their own photos, this dynamically
             * creates them on top of the photo.
             */
            val innerTickRadius = mCenterX - 10
            val outerTickRadius = mCenterX
            for (tickIndex in 0..11) {
                val tickRot = (tickIndex.toDouble() * Math.PI * 2.0 / 12).toFloat()
                val innerX = Math.sin(tickRot.toDouble()).toFloat() * innerTickRadius
                val innerY = (-Math.cos(tickRot.toDouble())).toFloat() * innerTickRadius
                val outerX = Math.sin(tickRot.toDouble()).toFloat() * outerTickRadius
                val outerY = (-Math.cos(tickRot.toDouble())).toFloat() * outerTickRadius
                canvas.drawLine(mCenterX + innerX, mCenterY + innerY,
                        mCenterX + outerX, mCenterY + outerY, mTickAndCirclePaint!!)
            }

            /*
             * These calculations reflect the rotation in degrees per unit of time, e.g.,
             * 360 / 60 = 6 and 360 / 12 = 30.
             */
            val seconds = mCalendar!!.get(Calendar.SECOND) + mCalendar!!.get(Calendar.MILLISECOND) / 1000f
            val secondsRotation = seconds * 6f

            val minutesRotation = mCalendar!!.get(Calendar.MINUTE) * 6f

            val hourHandOffset = mCalendar!!.get(Calendar.MINUTE) / 2f
            val hoursRotation = mCalendar!!.get(Calendar.HOUR) * 30 + hourHandOffset

            /*
             * Save the canvas state before we can begin to rotate it.
             */
            canvas.save()

            canvas.rotate(hoursRotation, mCenterX, mCenterY)
            canvas.drawLine(
                    mCenterX,
                    mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                    mCenterX,
                    mCenterY - sHourHandLength,
                    mHourPaint!!)

            canvas.rotate(minutesRotation - hoursRotation, mCenterX, mCenterY)
            canvas.drawLine(
                    mCenterX,
                    mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                    mCenterX,
                    mCenterY - sMinuteHandLength,
                    mMinutePaint!!)

            /*
             * Ensure the "seconds" hand is drawn only when we are in interactive mode.
             * Otherwise, we only update the watch face once a minute.
             */
            if (!mAmbient) {
                canvas.rotate(secondsRotation - minutesRotation, mCenterX, mCenterY)
                canvas.drawLine(
                        mCenterX,
                        mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                        mCenterX,
                        mCenterY - mSecondHandLength,
                        mSecondPaint!!)

            }
            canvas.drawCircle(
                    mCenterX,
                    mCenterY,
                    CENTER_GAP_AND_CIRCLE_RADIUS,
                    mTickAndCirclePaint!!)

            /* Restore the canvas' original orientation. */
            canvas.restore()

            /* Draw rectangle behind peek card in ambient mode to improve readability. */
            if (mAmbient) {
                canvas.drawRect(mPeekCardBounds, mBackgroundPaint!!)
            }

        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)

            if (visible) {
                registerReceiver()
                /* Update time zone in case it changed while we weren't visible. */
                mCalendar!!.timeZone = TimeZone.getDefault()
                invalidate()
            } else {
                unregisterReceiver()
            }

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer()
        }

        override fun onPeekCardPositionUpdate(rect: Rect?) {
            super.onPeekCardPositionUpdate(rect)
            mPeekCardBounds.set(rect)
        }

        private fun registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return
            }
            mRegisteredTimeZoneReceiver = true
            val filter = IntentFilter(Intent.ACTION_TIMEZONE_CHANGED)
            this@MyFirstWatchFace.registerReceiver(mTimeZoneReceiver, filter)
        }

        private fun unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return
            }
            mRegisteredTimeZoneReceiver = false
            this@MyFirstWatchFace.unregisterReceiver(mTimeZoneReceiver)
        }

        /**
         * Starts/stops the [.mUpdateTimeHandler] timer based on the state of the watch face.
         */
        private fun updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
            }
        }

        /**
         * Returns whether the [.mUpdateTimeHandler] timer should be running. The timer
         * should only run in active mode.
         */
        private fun shouldTimerBeRunning(): Boolean {
            return isVisible && !mAmbient
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        fun handleUpdateTimeMessage() {
            invalidate()
            if (shouldTimerBeRunning()) {
                val timeMs = System.currentTimeMillis()
                val delayMs = INTERACTIVE_UPDATE_RATE_MS - timeMs % INTERACTIVE_UPDATE_RATE_MS
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs)
            }
        }
    }

    companion object {

        /*
     * Update rate in milliseconds for interactive mode. We update once a second to advance the
     * second hand.
     */
        private val INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1)

        /**
         * Handler message id for updating the time periodically in interactive mode.
         */
        private val MSG_UPDATE_TIME = 0
    }
}
