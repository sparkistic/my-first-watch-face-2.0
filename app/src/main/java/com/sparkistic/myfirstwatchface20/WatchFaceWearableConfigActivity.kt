package com.sparkistic.myfirstwatchface20

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager

/**
 * The watch-side config activity which allows for setting the background dog.
 */
class WatchFaceWearableConfigActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_digital_config)

        findViewById(R.id.dog_one_button).setOnClickListener {
            sendMessageToWatchFace(DOGS.ERNIE)
            finish()
        }

        findViewById(R.id.dog_two_button).setOnClickListener {
            sendMessageToWatchFace(DOGS.LUCY)
            finish()
        }
    }

    private fun sendMessageToWatchFace(whichDog: DOGS) {
        val intent = Intent(DOG_CHANGE_EVENT)
        intent.putExtra("dog", whichDog.toString())
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    companion object {
        val DOG_CHANGE_EVENT = "DogChangeEvent"
    }
}